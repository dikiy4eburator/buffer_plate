# buffer PCB

stereo to 2.1 balanced buffer, with volume controls

power supply +/- 12V DC

<img src="opamp_buffer_balanced.png"
     style="float: left; margin-right: 10px;" />

<img src="opamp_buffer_lowpass.png"
     style="float: left; margin-right: 10px;" />
